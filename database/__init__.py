import pandas
import os
import RobotTools

conf_path = os.path.join(RobotTools.__path__[0], 'database', 'labware_configuration.csv')
labware_meta = pandas.read_csv(conf_path)

labware_meta.labware_type = [l.strip() for l in labware_meta.labware_type]

def process_item(item, **search_attributes):
    if len(item) == 0:
        msg = 'Cannot find item in database corresponding to search attributes: {}. Please edit the database file.'
        raise Exception(msg.format(search_attributes))
    elif len(item) > 1:
        msg = 'Found more than one item in databaes matching search attributes: {}. Please edit the database file.'
        raise Exception(msg.format(search_attributes))
    return item.iloc[0].to_dict()

def get_by_ID(ID):
    item = labware_meta[labware_meta.ID == ID]
    return process_item(item, ID=ID)

def get_by_labware_type(labware_type):
    item = labware_meta[labware_meta.labware_type == labware_type]
    return process_item(item, labware_type=labware_type)
