# Design considerations

## Modeling scripts

*. How to show the user the state of the system at every given time point?


## Communicating with instruments / drivers

*. One of the things that's unclear to me about the current design is what the role of a 'driver' is.
Specifically, do drivers just return text commands or should they figure out how to communicate this command to the proper location.

The drivers should probably be provided with a "pipe" into which to send their commands.

This pipe could be monitored by something that decides what to do with the commands; e.g., issue it now or later, issue it to a real robot or to a simulated one.

*. Say the robot gets connected to another instrument. Should the robot "know" about it? 

## Modeling labware

*. movable property for labware... Maybe we should more specifically say which devices
are allowed to move the labware. For example, tips are definitely movable, but it's the LiHa
arm that moves them not the RoMa.

*. grid property for labware. We need to remove it and replace it by references
to coordinates everywhere...

*. Modeling the tip as labware is very sensible.

*. How do we model wells? As LiquidSites?


# Liquid Profiles

specify default liquid classes for aspirate/dispense/mix actions

# Error messages to update

When liha tries to access a well that doesn't exist in the labware.

"ValueError: invalid entry in coordinates array"
