from workspace import Workspace
from scripter import Scripter

class Robot(object):
    def __init__(self, scripting_mode='offline'):
        self.workspace = None
        self.scripter = Scripter(mode=scripting_mode)
        self.devices = []
        self.connect_arms()
        self.connect_devices()

    def connect_devices(self):
        print 'The device configuration is hard-coded at the moment.'
        device_list = []
        from devices import PlateReader
        from drivers.evoware import sunrise_plate_reader
        from sites import LabwareSite

        self.plate_reader = PlateReader('Sunrise', sunrise_plate_reader, 0)
        self.plate_reader._add_observer(self.scripter)

        self._plate_reader_site = LabwareSite(1, self, allowed_labware='all', coordinates=3)
        self._plate_reader_site.place_labware(self.plate_reader)

        self.devices.extend([self.plate_reader])

    def connect_arms(self):
        """
        Connects the robot arms.
        """
        print 'NOTE: Arm connection is hard coded'
        from devices.evoware import EvowareRoMa, EvowareLiHa
        from drivers.evoware import roma, liha
        self.arms = [EvowareRoMa('RoMa', roma, 0), EvowareLiHa('LiHa', liha, 0, (8, 1))]
        self.roma = self.arms[0]
        self.liha = self.arms[1]

        for arm in self.arms:
            arm._add_observer(self.scripter)

    def save_script(self, output_filename):
        self.workspace.to_esc_file(output_filename)
        self.scripter.save(output_filename, 'a') # Append mode

    def load_workspace(self, filename):
        """
        Parameters
        ------------
        filename : str
            name of .esc file to load hardware model
        """
        self.workspace = Workspace.from_esc_file(filename)
