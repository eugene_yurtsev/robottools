"""
Utility functions for working with pipettor devices.
Still need to find the proper place for this.
"""
from RobotTools.core.doc import doc_replacer
import numpy
from string import ascii_uppercase
import collections

def to_list(x):
    if not isinstance(x, collections.Iterable):
        return [x]
    else:
        return x

def decode_site(site, shape, order, input_type):
    """
    Given a site offset (int) or a site location (.e.g., 'A2')
    Returns a 2-tuple corresponding to the multi-index

    This function will need to be adjusted if the shape is every larger than 'Z'
    """
    if isinstance(site, basestring):
        if site[0] not in ascii_uppercase or site[1] in ascii_uppercase:
            raise NotImplementedError('Position indexing for site of form "{}" was not implemented'.format(site))
        return (ord(site[0]) -ord('A'), int(site[1:])-1)
    else:
        if input_type == 'offset':
            site_number = site
        else:
            site_number = site - 1
        return numpy.unravel_index(site_number, shape, order=order)

@doc_replacer
def generate_site_selection(site_selection, shape, num_sites,
        site_step=1, increment_order='row', input_type='offset'):
    """
    Generates a list of row_indexes and column_indexes corresponding to the
    desired wells.

    Parameters
    -----------
    {_site_selection_options}

    input_type : ['offset', 'position']
        This is only important when site_selection was given as int or list of ints.
        Use 'offset' if input is offsets; for example site_selection=0, corresponds to 'A1'
        Use 'position' if input is positions; for example site_selection=1, corresponds to 'A1'.

    Returns
    -----------
    position_matrix
        position_matrix[0] -> row indexes
        position_matrix[1] -> col indexes
    """
    if input_type not in ('offset', 'position'):
        raise ValueError('input_type cannot be {}'.format(input_type))
    try:
        max_rows, max_columns = shape
        increment_order = 'F' if increment_order == 'row' else 'C'

        # Let's convert strs to positions on the plate.
        # Here counting from 0 everywhere!
        if site_selection == 'all':
            num_sites = numpy.product(shape)
            site_mat = numpy.unravel_index(range(num_sites), shape, increment_order)
            site_mat = numpy.array(site_mat)
        else:
            only_starting_well_was_given = True if not isinstance(site_selection, (list, numpy.ndarray)) else False

            if only_starting_well_was_given:
                starting_well = decode_site(site_selection, shape, increment_order, input_type)
                starting_well = numpy.ravel_multi_index(starting_well, shape,
                        order=increment_order)
                well_indexes = numpy.arange(num_sites) * site_step + starting_well
                site_mat = numpy.unravel_index(well_indexes, shape,
                        order=increment_order)
                site_mat = numpy.array(site_mat)
            else:
                site_mat = zip(*map(lambda x : decode_site(x, shape, increment_order, input_type), site_selection))
                site_mat = numpy.array(site_mat)
    except ValueError as e:
        new_msg = 'The following exception was encountered {}\n'.format(e.message)
        new_msg += 'Likely because more sites/tips were specified than is possible for the given labware/device.' +\
                '\nAlso please note that numbers are considered as offset relative to position "A1", so counting is from 0 rather than 1.'
        raise ValueError(new_msg)

    return site_mat


if __name__ == '__main__':
    ##
    # Test for generate site_selection. DO NOT REMOVE. Push into tests if you have time.
    # The operation of this function is very important!
    arr1 = generate_site_selection('C1', (4, 3), 3).T
    arr2 = generate_site_selection(2, (4, 3), 3).T
    arr3 = generate_site_selection(['C1', 'D1', 'A2'], (4,3), 1).T
    arr4 = generate_site_selection([2, 3, 4], (4,3), 1).T

    target = numpy.array([[2, 0], [3, 0], [0, 1]])

    data_to_compare = [arr1, arr2, arr3, arr4]

    for data in data_to_compare:
        if any((target != data).flatten()):
            print 'ERROR'
        else:
            print 'Success'

    print generate_site_selection('all', (8, 1), 8).T
    print generate_site_selection(1+numpy.arange(8), (8, 1), 'ignore', input_type='position')

    try:
        print generate_site_selection(4, (8, 1), 8).T
    except ValueError:
        print 'Something that should have failed has failed.'
    print generate_site_selection('AB1', (8, 1), 8).T
    # This should fail



