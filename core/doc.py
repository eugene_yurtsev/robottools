"""
Contains documentation for RobotTools
"""

from GoreUtilities import docstring


_doc_dict = dict(

_site_selection_options="""\
site: int | str | list of str
    e.g., ['A3', 'G1'] etc.
    NOTE: INT TYPE NOT IMPLEMENTED PROPERLY
    sites can be either wells or tips.
shape : tuple
    dimensions of labware container
increment_order : 'row' | 'col'
    direction of counting wells
site_step : int
    site_step = 2, then generated sequence could be 'A1', 'C1', 'E1', 'H1', etc.""",

_tip_formatting_options="""\
tips : str | list
    '00110011', uses tip 3, 4, 7, 8
    'all', uses all tips,
    list : [1, 3, 4], uses tips 1, 3, 4""",

_volume_formatting_options="""\
volumes: [int | [int]]
    volume in ul
    if int, same volume is used for each tip
    if list of int, then specifies volume for each tip""",

)

doc_replacer = docstring.DocReplacer(**_doc_dict)
doc_replacer.replace()

