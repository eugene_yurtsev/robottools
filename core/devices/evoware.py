"""
Extends some of the generic drivers to include some of evowares strange ways
of doing things. :)
"""
from base import Mover, Pipettor

class EvowareRoMa(Mover):
    def transfer_and_uncover_lid(self, labware, labware_destination_site,
            lid_destination_site, **command_kwargs):
        """
        Transfer Labware

        Parameters
        ----------
        labware: Labware
            Which labware to move
        destination_site: LabwareSite
        command_kwargs : other keyword arguments to be passed to the command.
        """
        if not labware.covered:
            raise Exception('Labware {} is not covered with a lid.'.format(labware))
    	lid = labware.lid_site.labware
        lid_destination_site.check_availability(lid)
        labware_destination_site.check_availability(labware)

        self._issue_command('transfer', labware=labware,
                destination_site=labware_destination_site, lid_destination_site=lid_destination_site, **command_kwargs)

        lid_destination_site.place_labware(lid)
        labware_destination_site.place_labware(labware)

    def cover_with_lid_and_transfer(self, labware, destination_site, lid,
            **command_kwargs):
        """
        Transfer Labware

        Parameters
        ----------
        labware: Labware
            Which labware to move
        destination_site: LabwareSite
        command_kwargs : other keyword arguments to be passed to the command.
        """
        labware.lid_site.check_availability(lid)
        destination_site.check_availability(labware)

        self._issue_command('transfer', labware=labware, lid=lid,
                destination_site=destination_site, **command_kwargs)

        labware.lid_site.place_labware(lid)
        destination_site.place_labware(labware)

class EvowareLiHa(Pipettor):
    def move_above_labware(self, labware, **command_kwargs):
        """ moves the liha above the labware """
        tips = self._format_tips_as_tip_list(1) # first tip, counting from 1, not zero
        command_kwargs['movement_offset'] = 0 # no offset
        command_kwargs['movement_type'] = 0 # global z-travel
        command_kwargs['move_speed'] = 10
        command_kwargs['target_position'] = 0 # z-travel
        self._issue_command('move_liha', labware=labware, tips=tips, wells='A1', **command_kwargs)
