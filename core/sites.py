"""
Need to refactor and combine sites together.
Include information on which devices can access which site? This may be too much.
"""

class LabwareSite(object):
    """
    Carriers can have multiple Labware Site.
    The role of a labware site is to track whether it is occupied and if so by what labware.
    """
    def __init__(self, site_number, carrier, allowed_labware=None, coordinates=None):
        self.site_number = site_number
        self.allowed_labware = allowed_labware
        self.carrier = carrier

        self.labware = None
        self._coordinates = coordinates

    @property
    def coordinates(self):
        if self._coordinates is not None:
            return self._coordinates
        else:
            return self.carrier.coordinates

    @property
    def name(self):
        """
        The name is a property because the grid can change if the carrier is moved!
        """
        ##
        # QUICK HACK BECAUSE WORKSPACE HAS NO LABWARE_TYPE
        # Something is confused in terms of design... :(
        if hasattr(self.carrier, 'labware_type'):
            return 'Site #: {}, Carrier Type {}, Grid: {}'.format(self.site_number,
                    self.carrier.labware_type, self.grid,)
        else:
            return 'Site #: {}, Grid: {}'.format(self.site_number, self.grid)

    @property
    def grid(self):
        """ Finds grid position on robot where labware is located. """
        return self.coordinates

    def check_availability(self, labware):
        """
        Raises error if site is cannot be occupied by labware.

        TODO: Change to returning False and True with some explanation?

        Parameters
        ------------
        labware : Labware instance
        """

        if self.allowed_labware is None or \
            (labware.labware_type not in self.allowed_labware and self.allowed_labware != 'all'):
            msg = 'labware {} is not allowed on site {} of carrier {}'
            raise Exception(msg.format(labware, self, self.carrier))

        if (self.labware is None) or (self.labware is labware):
            if self.labware is labware:
                print('WARNING: Labware {} is already occupying site {}'.format(labware, self))
            return True
        else:
            raise Exception('Labware site is already occupied!\
                    It holds labware {}'.format(self.labware))

    def place_labware(self, labware):
        """ Places the labware in the labware site.

        ATTENTION: This design assumes that a labware can only occupy
        one labware site. This is not true on the actual robot since
        labware can be large and block access to nearby sites.
        Let's avoid modeling this complexity for the time being.
        Since we'll try to avoid doing collision detection and rely
        on the user to be smart.

        Returns
        --------
        bool
            True if successful.
            Otherwise raises an error.
        """
        self.check_availability(labware)

        ## Warning order of operation is important below.
        # Clear original site
        previous_site = labware.site

        if previous_site is not None:
            # It can be none when a new labware is added to the workspace.
            previous_site.labware = None # Clear original site

        # Set labware for this site
        self.labware = labware
        # Set new site for labware
        labware.site = self

    def short_description(self):
        return 'Site: #{}\t\tHolds Labware: {}'.format(self.site_number, self.labware)

    def __str__(self):
        return '{}\n\tAllowed Labware: {}'.format(self.short_description(),
                self.allowed_labware)

    def __repr__(self):
        return self.__str__()

## 
# Not implemented yet
#class LiquidSite(object):
    #def __init__(self, xpos, ypos, plate, filledVolume = 0):
        #self.xpos = xpos
        #self.ypos = ypos
        #self.plate = plate
        #self.filledVolume = filledVolume
#class TipSite(object):
    #def __init__(self, shape):
        #self.shape = shape
#
    #def _tip_loc_to_index(self, tip_mat):
        #"""
        #Works with output of _format_tips_as_tip_list
        #To be used internally
        #"""
        #return numpy.ravel_multi_index(tip_mat, self.pipettor_shape)
