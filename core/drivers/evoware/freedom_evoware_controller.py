"""
Implements a controller for the EVOware software

Notes:

Somehelpful things to keep in mind when working with pyevo the STATUS returned
by getSTATUS method is a binary number.  It is the bit sum of all current
statutes (it encodes multiple machine statuses (statii?)) at the same time.
This is why in the code below there is a bit wise & when inquiring the machine
status.
"""
from win32com.client import DispatchWithEvents
from fe_controller_util import EVOConstants
import time
import os

class EVOSoftwareController(object):
    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password
        self.system = DispatchWithEvents('EVOAPI.System', EVOSystemEvents)
        self.script = DispatchWithEvents('EVOAPI.Script', EVOScriptEvents)

    def login(self, mode='simulation'):
        """ Logs the robot in """
        mode_options = ('simulation', 'real')
        if mode not in mode_options:
            raise ValueError('mode must be in {}'.format(mode_options))
        real_mode = 0 if mode == 'simulation' else 1
        self.system.Logon(self.username, self.password, 0, real_mode)

        current_status = self.system.GetStatus()
        while self.status_is(EVOConstants.STATUS_LOADING):
            time.sleep(1)
        print "System Ready"

    def status_is(self, status):
        if self.system.GetStatus() & status == status:
            return True
        else:
            return False

    def initialize(self):
        if self.status_is(EVOConstants.STATUS_NOTINITIALIZED):
            print 'Initializing'
            self.system.Initialize()
            print 'Initialized'
        else:
            print 'System already initialized'

    def logout(self):
        """ Logs the robot out """
        self.system.Logoff()

    def shutdown(self):
        """ Shuts down the software. """
        self.logout()
        self.system.Shutdown()

    def execute_command(self,command):
        """ Executes a command. """
        self.system.ExecuteScriptCommand(command)

    def load_workspace(self, esc_file):
        """ Path to .esc file. """
        if not os.path.isabs(esc_file):
            esc_file = os.path.abspath(esc_file)
        self.scriptID = self.system.PrepareScript(esc_file)

    def run_current_script(self):
        self.system.StartScript(self.scriptID)

    def pause(self):
        self.system.Pause()

    def stop(self):
        self.system.Stop()

    def resume(self):
        self.system.resume()

class EVOSystemEvents(object):
    """ System events handler """

    def OnStatusChanged(self, status):
        """method StatusChanged"""
        #helper for colored status messages
        def debug(message):
	    print 'EVOSystem Event: '+ message
        statusString = ""
        for key,value in EVOConstants.STATUS.items():
            if status & key == key:
                statusString += value
        debug("STATUS: " + statusString)

    def OnUserPromptEvent(self, ID, text, caption, choices, answer):
        print "UserPrompt" + text
        print "Choices:"
        print choices

    def OnErrorEvent(*args):
        print "System Message (May be error, but may be OK):"
        print args

class EVOScriptEvents(object):
    """ Script events handler """
    def OnStatusChanged(self, status):
        """method StatusChanged"""
        #helper for colored status messages
        def debug(message):
            #color message red when printing
            #print '\033[1;31m' + message + '\033[1;m'
            print message
        statusString = ""
        for key,value in EVOConstants.STATUS.items():
            if status & key == key:
                statusString += value
        debug("STATUS: " + statusString)

    def OnUserPromptEvent(self, ID, text, caption, choices, answer):
        print "UserPrompt" + text
        print "Choices:"
        print choices

    def OnErrorEvent(*args):
        print "Error:"
        print args
