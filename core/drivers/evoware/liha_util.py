"""
Utility functions for liha
"""
from RobotTools.core.doc import doc_replacer
import numpy

LIHA_TIPS = 8 ## NEED TO REFACTOR INTO CLASS
MAX_TIPS_FOR_COMMANDS = 12 # Hardcoded in evo software for some reason.

def _get_single_tip_encoding(tip_number):
    """ Do not use. Just a helper method for get_tip_encoding. """
    if tip_number >= LIHA_TIPS:
        raise ValueError('tip number must be between 1 and {}'.format(LIHA_TIPS))
    return 1 << (tip_number)

def get_tip_encoding(tips):
    """
    Calculates the tip encoding
    list : [1, 3, 4], uses tips 1, 3, 4

    source: Modified code from pyevo repository.
    """
    if not isinstance(tips, list):
        raise TypeError('Tips must be a list.')

    encoding = 0

    for tip in tips:
        encoding += _get_single_tip_encoding(tip)
    return encoding

def get_tip_volume_encoding(tips, volume):
    """
    Returns a string describing the volumes to pipette with each tip.

    Parameters
    ------------
    tips : list
    volume : int | list

    Returns
    ----------
    str
        '0, 0, "100", "100"' -> Volume of 100 for tip 3 and 4.

    source: Modified code from pyevo repository.
    """
    if isinstance(volume, (int, float)):
        volume = [volume for i in range(len(tips))]

    if len(volume) != len(tips):
        raise ValueError('The volume list does not match the tip list in length')

    tip_volume_map = dict(zip(tips, volume))

    encoding = ""

    for i in range(MAX_TIPS_FOR_COMMANDS):
        vol = tip_volume_map.get(i, 0)
        if vol == 0:
            encoding += '0'
        else:
            encoding += '"{}"'.format(vol)

        if not i == (MAX_TIPS_FOR_COMMANDS - 1):
            encoding += ","
    return encoding

@doc_replacer
def get_well_selection_encoding(row_list, col_list, shape):
    """
    Please read section A.15.3 in from EVOWare documentation about Well
    Selection Encoding

    Parameters
    ----------
    {_site_selection_options}

    NOTE: Most of these are relevant only when wells is not a list.
    ATTENTION: Counting from zero everywhere here!

    Returns
    ----------
    well encoding string

    source: Modified code from pyevo repository.
    """
    max_rows, max_columns = shape
    selString = '{0:02X}{1:02X}'.format(max_columns, max_rows)
    bitCounter = 0
    bitMask = 0

    # NOTE: There may be a way to speed up this function. At the moment
    # it just does a double for loop.
    for x in range(max_columns):
        for y in range(max_rows):
            if x in col_list and y in row_list:
                bitMask |= 1 << bitCounter
            bitCounter += 1
            if bitCounter > 6:
                selString += chr(ord('0') + bitMask)
                bitCounter = 0
                bitMask = 0
    if bitCounter > 0:
        selString += chr(ord('0') + bitMask)
    return '{}'.format(selString)

if __name__ == '__main__':
    print 'simple tests'
    print ('0C0810000000000000' == get_well_selection_encoding([0], [0] , (8, 12)))
    print ('0C08@0000000000000' == get_well_selection_encoding([4], [0], (8, 12)))
    print ('0C08A0000000000000' == get_well_selection_encoding([0, 4], [0, 0], (8, 12)))



