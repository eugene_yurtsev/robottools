"""
Driver for EVOWares LiHa
"""
from liha_util import get_well_selection_encoding
from liha_util import get_tip_encoding, get_tip_volume_encoding
from RobotTools.core.util import generate_site_selection
import warnings

command_set_map = {
        ## 
        # These commands match the commands issues in EVOware freedom worklists. 
        # Please see evoware documentation for worklists.
        # EVOware documentation section A.15
        'aspirate'       : 'Aspirate({tip_mask},{liquid_class},{tip_volume},{grid},' + \
                          '{site},{tip_spacing},"{well_encoding}",0,0);',
        'dispense'       : 'Dispense({tip_mask},{liquid_class},{tip_volume},{grid},' + \
                          '{site},{tip_spacing},"{well_encoding}",0,0);',
        'mix'            : 'Mix({tip_mask},{liquid_class},{tip_volume},{grid},' + \
                          '{site},{tip_spacing},"{well_encoding}",{mixing_cycles},0,0);',
        'drop_tips'       : 'DropDITI({tip_mask},{grid},{site},{airgap}, {airgap_speed}, {arm});',
        'pick_up_tips'   : 'PickUp_DITIs2({tip_mask},{grid},{site},"{well_encoding}",' + \
                                '0,{DITI_type},0);',
        'put_down_tips' : 'Set_DITIs_Back({tip_mask},{grid},{site},"{well_encoding}",' + \
                            '0,0);',

        ##
        # We'll implement our own equivalent of the two commands below
        'pick_up_tips_automatically' : 'GetDITI2({tip_mask},"{tip_type}", 0, 0, {air_volume}, {air_speed});',
        'move_liha' : 'MoveLiha({tip_mask},{grid},{site},{tip_spacing},"{well_encoding}",'+\
                '{movement_type},{target_position},{movement_offset},{move_speed},0,0);'

        ## Move liha explanation
        #

        # movement_type
        # 0 = positioning with global z-travel 
        # 1 = positioning with local z-travel 
        # 2 = x-move 
        # 3 = y-move 
        # 4 = z-move

        # target_position (position after move): 
        # 0 = z-travel 
        # 1 = z-dispense 
        # 2 = z-start 
        # 3 = z-max 
        # 4 = global z-travel 

        # movement offset
        # offset in mm added to target_position (parameter 7)
        }

def generate_command(device, command, **kwargs):
    """
    Generates LiHa pipetting commands.
    """
    command = command_set_map[command]

    ## Preparing the command parameters
    command_pars = { 'arm' : device.ID}

    if device.ID != 0:
        raise warnings.warn('Device ID is not 0. The driver parameters likely need to be adjusted.')

    if 'labware' in kwargs:
        labware = kwargs.pop('labware')
        command_pars.update({
            'grid' : labware.grid,
            'site' : labware.site.site_number - 1,
        })

    if 'DITI_type' in command:
        command_pars['DITI_type'] = '"{}"'.format(labware.labware_type)

    if 'tips' in kwargs:
        tips = kwargs.pop('tips')
        tips = list(tips[0]) # This is specifically for the LiHa
        command_pars['tip_mask'] = get_tip_encoding(tips)

    if 'volumes' in kwargs:
        volumes = kwargs.pop('volumes')
        command_pars['tip_volume'] = get_tip_volume_encoding(tips, volumes)

    if 'wells' in kwargs:
        num_wells = len(tips)
        wells = kwargs.pop('wells')
        well_step = kwargs.pop('well_step', 1)
        increment_order = kwargs.pop('increment_order', 'row')


        ## NEED TO REFACTOR. This shouldn't be done by the driver.
        well_selection = generate_site_selection(wells,
                    labware.shape, num_sites=num_wells, site_step=well_step,
                    increment_order=increment_order)

        # Well encoding
        well_encoding = get_well_selection_encoding(well_selection[0], well_selection[1], labware.shape)

        command_pars.update({
                'well_encoding' : well_encoding,
            })

    if 'liquid_class' in kwargs:
        liquid_class = kwargs.pop('liquid_class')
        if isinstance(liquid_class, str):
            kwargs['liquid_class'] = '"{}"'.format(liquid_class)
        else:
            kwargs['liquid_class'] = '"{}"'.format(liquid_class.name)

    # Update with whatever additional parameters might have been provided in kwargs
    command_pars.update(kwargs)

    ## DITI_fetch_options
    # 0, If DITI is not fetched the first time, raise error
    # 1, try 3 times then move to next position
    command_pars.setdefault('DITI_fetch_options', 0)

    # For get DITI command
    command_pars.setdefault('air_volume', 10) # ul
    command_pars.setdefault('air_speed', 70) # ul/sec
    command_pars.setdefault('tip_spacing', 1) # Not sure what this does yet.
    # Format output command
    command = command.format(**command_pars)

    return command

def pick_up_tips(**kwargs):
    return generate_command(command='pick_up_tips', **kwargs)

def pick_up_tips_automatically(**kwargs):
    return generate_command(command='pick_up_tips_automatically', **kwargs)

def put_down_tips(**kwargs):
    return generate_command(command='put_down_tips', **kwargs)

def drop_tips(**kwargs):
    return generate_command(command='drop_tips', **kwargs)

def dispense(**kwargs):
    return generate_command(command='dispense', **kwargs)

def mix(**kwargs):
    return generate_command(command='mix', **kwargs)

def aspirate(**kwargs):
    return generate_command(command='aspirate', **kwargs)


def move_liha(**kwargs):
    return generate_command(command='move_liha', **kwargs)

if __name__ == '__main__':
    pass
    #device.ID = 1
#
    #dispense(device=device, test='test')
    #LiHa.generate_command('test', command='dispense')

