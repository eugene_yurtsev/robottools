"""
General purpose wizards.
"""
from numpy import array
from ..core.util import to_list

def transfer_liquid(pipettor, volume, source, source_well, destination, destination_well,
                    liquid_profile, tip_boxes, tip=[1], reuse_tips_when_possible=True):
    """
    Transfers arbitrary amount of liquid from source well to destination well using the given pipettor.

    Attention: Pipettor should not have any tips on before calling this function.

    Parameters
    --------------
    pipettor: Pipettor
        (e.g., LiHa)
    source : PipettorSite
    destination : PipettorSite
    liquid_profile : dict
        Specifies which liquid profile to use for aspiration and dispense
        must contain the keys : 'aspirate', 'dispense'
    tip_boxes : [list of TipBox]
    tip : int [1-8]
        Specifies which tip to use. For example, [1]. Will use the first tip.
    reuse_tips_when_possible : [True | False]
        If True, the pipettor can reuse the same tip to aspirate from the source and dispense into the destination multiple times.
    """
    total_volume = volume
    volume_left = total_volume

    tip_capacities_type_map = set([(x.tip_capacity, x.labware_type) for x in tip_boxes])
    tip_capacities_type_map = {x[0] : x[1] for x in tip_capacities_type_map}
    available_tip_capacities = array(tip_capacities_type_map.keys())

    # Current loaded tip type
    current_tip_type = None

    ## Algorithm
    while volume_left > 0:
        indexes = to_list(available_tip_capacities >= volume_left)

        if any(indexes):
            # Pipette entire volume that's left
            valid_capacities = available_tip_capacities[indexes]
            valid_capacities = to_list(valid_capacities)
            capacity_to_use = min(valid_capacities)

            volume_to_pipette = volume_left
        else:
            # Pipette maximum possible volume
            capacity_to_use = max(available_tip_capacities)
            volume_to_pipette = capacity_to_use

        volume_left = volume_left - volume_to_pipette
        tipbox_lawbare_type = tip_capacities_type_map[capacity_to_use]

        # drop / pick up tips
        if (current_tip_type is None) or (current_tip_type != tipbox_lawbare_type) or (not reuse_tips_when_possible):
            if current_tip_type is not None:
                # This if statement is to prevent dropping tips before picking up tips for the first time.
                pipettor.drop_tips(tip)
            pipettor.pick_up_tips_automatically(tip, tipbox_lawbare_type)
            current_tip_type = tipbox_lawbare_type

        # Transfer volumes
        pipettor.aspirate(tip, volume_to_pipette, source, source_well, liquid_class=liquid_profile['aspirate'])
        pipettor.dispense(tip, volume_to_pipette, destination, destination_well, liquid_class=liquid_profile['dispense'])

    # Drop tip
    if current_tip_type is not None:
        # Drop tips only if tips have been picked up
        pipettor.drop_tips(tip)
